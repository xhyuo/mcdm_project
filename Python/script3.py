# Author: Jordi Serra-Musach

from script0 import *
import pandas as pd

_dir = Dir()

## Three datasets will be loaded:
##   1.- drugs info with targets (symbol and entrezid)
##   2.- The ortholog file conversion
##   2.- ENSMBL ids --> mgi ids

## Drugs info
D = pd.read_csv(_dir.data + "drugs_info.csv")
D.columns = ['symbol_human', 'Drug_IDs', 'type', 'entrez_id_human']
D["symbol_human_lower"] = D["symbol_human"].apply(lambda v: v.lower())

## Ortholgos
T = pd.read_csv(_dir.data +  "Orthologues_human_mouse_2017_07_03.txt")
T["HumanGeneName_lower"] = T["HumanGeneName"].apply(lambda v: v.lower())
## ensmbl to mgi
nfit = (_dir.HOME + "workbench" + "string" + "20180508_string_10090" +
        "data" + "biomart_20180808" + "biomart_20180508.tsv")
M = pd.read_csv(nfit, sep = "\t")
M.columns = ['GeneStableID_mouse', 'ProteinStableID_mouse', 'NCBIgeneID_mouse', 'MGI_ID_mouse', 'MGI_symbol_mouse']

## In order to have the
## drug target (D) --> ortholog (T) --> ensembls Protein and mgi ids (M)
## we will merge D, T and M in the right way

TM = pd.merge(T, M, left_on = "MouseGeneStableID", right_on = "GeneStableID_mouse")


## joining with human targets
TMD = pd.merge(TM, D, left_on = "HumanGeneName_lower", right_on = "symbol_human_lower")

X = TMD[["Drug_IDs", "ProteinStableID_mouse", "MGI_ID_mouse", "MGI_symbol_mouse"]]
X.drop_duplicates(inplace = True)

X.to_csv(_dir.data + "drug_to_ortholog_targets.dat", sep = "\t", index = False)
