# Author: Jordi Serra-Musach

from script0 import *
import networkx as nx
import ezodf


_dir = Dir()
X0 = pd.read_excel(_dir.data + "RA_smoothed" + "allcellssick.xls", sheetname = None)
X = X0["up_regulators_granulocytes"]
z = np.array(map(lambda x: not isinstance(x, float), X.iloc[:,0]))
X = X.loc[z,]

G = nx.MultiGraph()
