# Author: David Martínez Enguita

from script0 import *
import pandas as pd
import networkx as nx
import pylab as plt
from scipy import stats


nx.graphviz_layout = nx.nx_pydot.graphviz_layout
_dir = "/home/dme/Documents/MCDM_project/Python_scripts/data/"
X = pd.read_excel(_dir + "/human_RA.xls", sheet_name = "network", index_col = None)

## Randow Walk Score
new_edgs = []

for i in range(len(X)):
    u = X.From[i]
    v = X.To[i]
    w = X.weight[i]
    new_edgs += [(u, v, w)]

G = nx.Graph()
G.clear()
for u, v, w in new_edgs:
    G.add_edge(u, v, nu = w)

Cr =  list(nx.current_flow_betweenness_centrality(G, weight = "nu").items())
Cr.sort(key = lambda v: v[1], reverse = True)
Cr = pd.DataFrame(Cr)
Cr.columns = ["cell", "prob"]

res = pd.DataFrame()
res["rw_score"] = Cr["prob"]
res.index = Cr["cell"]
res["ncell"] = 0
           
res.to_csv(_dir + "/NEW_RESULTS/random_walk_score_human_RA.tsv", sep = "\t")

## Communicability Centrality
d_nds = dict([v[::-1] for v in enumerate(G.nodes())])
aux = [(v[0], v[1], v[2]["nu"]) for v in G.edges(data = True)]

A = np.zeros([G.order()] * 2)
for u, v, w in aux:
    i = d_nds[u]
    j = d_nds[v]
    A[i, j] = A[j, i] = w
D = A.sum(0)

def f_norm(i, j):
    global D, A
    d = (D[i] * D[j]) ** (0.5)
    return 1. / d
    return 1. * A[i, j] / d

for i in range(A.shape[0]):
    for j in range(A.shape[1]):
        A[i,j] *= f_norm(i, j)

L, U = np.linalg.eigh(A)
U = np.array(U)

SC = (U ** 2) * np.exp(L)
SC = [SC[i,:].sum() for i in range(SC.shape[0])]

SC = pd.DataFrame(SC, index = G.nodes())
SC.columns = ["comm"]
SC.sort_values("comm", inplace = True, ascending = False)

res_com = pd.DataFrame()
res_com["SC"] = SC["comm"]
res_com.index = SC.index
res_com["ncell"] = 0

res_com.to_csv(_dir + "/NEW_RESULTS/subgraph_centrality_human_RA.csv", sep = "\t")

