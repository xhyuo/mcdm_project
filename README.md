R 3.6.0 and Python 3.7 scripts corresponding to the Multicellular Disease Models (MCDM) Project.

Code written by Jordi Serra-Musach and David Martínez Enguita.